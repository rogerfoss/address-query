package exercise.query;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class AdressController {
	
	@GetMapping("/{cep}")
	public String treatJson(RestTemplate restTemplate, @PathVariable String cep) {
		Adress adress = restTemplate.getForObject(
				String.format("https://viacep.com.br/ws/%s/json/", cep),
				Adress.class);
		
		return String.format("Adress [Cep: %s - Logradouro: %s - Bairro: %s - Localidade: %s - UF: %s]", adress.getCep(),
			adress.getLogradouro(), adress.getBairro(), adress.getLocalidade(), adress.getUf());
			
	}

}
